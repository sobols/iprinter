import model
import os
import re
import subprocess

current_dir = os.path.dirname(os.path.abspath(__file__))


def _get_rules(session):
    DbPCR = model.DbPrinterChoosingRule
    return session.query(DbPCR.hostname_regexp, DbPCR.printer).all()


def _resolve_printer(rules, printout):
    if printout.hostname is not None:
        printout.cur_printer = None
        for hostname_regexp, printer in rules:
            m = re.match('^' + hostname_regexp + '$', printout.hostname)
            if m is not None:
                printout.cur_printer = printer
                break


def resolve_all_printers(session):
    rules = _get_rules(session)
    for printout in session.query(model.DbPrintout):
        _resolve_printer(rules, printout)


def resolve_one_printer(session, printout):
    rules = _get_rules(session)
    _resolve_printer(rules, printout)


def filename_for_printout(printout):
    return '{:05d}.txt'.format(printout.id)


def try_to_print(printout):
    if printout.cur_printer is not None:
        subprocess.check_call('notepad.exe /PT "{0}" "{1}"'.format(os.path.join(current_dir, 'printouts', filename_for_printout(printout)), printout.cur_printer))
    printout.used_printer = printout.cur_printer


def make_header(printout):
    lines = []
    lines.append(u'Workstation: {0}'.format(printout.hostname))
    lines.append(u'Timestamp: {0}'.format(printout.timestamp.strftime('%d.%m.%Y %H:%M:%S')))
    lines.append(u'=' * 80)
    return ''.join([line + '\r\n' for line in lines])

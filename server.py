#!/usr/bin/env python
# -*- coding: utf-8 -*-

import cherrypy
import os
import socket
import datetime
import re
from mako.lookup import TemplateLookup

import common
import model
from cherrypy_sqlalchemy_stuff import SAEnginePlugin, SATool

current_dir = os.path.dirname(os.path.abspath(__file__))
MAX_PRINTOUT_LENGTH = 64 * 1024


class IPrinterError(Exception):
    pass


class IPrinterServer(object):
    def __init__(self):
        self._lookup = TemplateLookup(directories=['./'], input_encoding='utf-8', output_encoding='utf-8', default_filters=['h'])
        self.enabled = True

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("iprinter")

    @cherrypy.expose
    def iprinter(self, **params):
        template = self._lookup.get_template('index.html')
        text = params.get('textinput')

        try:
            if not self.enabled:
                raise IPrinterError(u'Service is not available now, please try again later. Sorry for inconvenience.')

            if len(params) == 0:
                return template.render(mode='new')
            elif len(params) == 1 and text is not None:

                if len(text) == 0:
                    raise IPrinterError(u'Nothing to print.')
                elif len(text) > MAX_PRINTOUT_LENGTH:
                    raise IPrinterError(u'Too large text: {0:,} characters. The limit for one printout is {1:,} characters.'.format(len(text), MAX_PRINTOUT_LENGTH))

                ip_address = cherrypy.request.headers["Remote-Addr"]
                try:
                    hostname, aliaslist, ipaddrlist = socket.gethostbyaddr(ip_address)
                    hostname = hostname.lower()
                except socket.error:
                    hostname = None

                session = cherrypy.request.db

                printout = model.DbPrintout(ip_address=ip_address, hostname=hostname, length=len(text), timestamp=datetime.datetime.now())
                session.add(printout)
                session.commit()

                text = common.make_header(printout) + text
                file_name = os.path.join('printouts', common.filename_for_printout(printout))
                with open(file_name, 'wb') as file:
                    file.write(text.encode('utf-8'))

                common.resolve_one_printer(session, printout)
                common.try_to_print(printout)
                session.commit()
                return template.render(mode='ok')
            else:
                raise IPrinterError(u'Unsupported parameters were passed to iPrinter.')

        except IPrinterError as e:
            return template.render(mode='error', message=unicode(e))


def parse_printer_list(lst):
    res = []
    for line in lst.split('\n'):
        line = line.rstrip('\r')
        if not line:
            continue
        regexp, name = line.split(' ', 1)
        r = re.compile(regexp)
        assert (r is not None)
        res.append((regexp, name))
    return res


class AdminServer:
    def __init__(self, iprinter_server):
        self._lookup = TemplateLookup(directories=['./'], input_encoding='utf-8', output_encoding='utf-8', default_filters=['h'])
        self._iprinter_server = iprinter_server

    @cherrypy.expose
    def index(self):
        raise cherrypy.HTTPRedirect("main")

    @cherrypy.expose
    def main(self, action=None, printer_list='', printout_id=''):
        session = cherrypy.request.db

        if action == 'stop':
            self._iprinter_server.enabled = False
        elif action == 'start':
            self._iprinter_server.enabled = True
        elif action == 'update_list':
            try:
                lst = parse_printer_list(printer_list)
                good_list = True
            except:
                good_list = False

            if good_list:
                session.query(model.DbPrinterChoosingRule).delete()
                for regexp, name in lst:
                    session.add(model.DbPrinterChoosingRule(hostname_regexp=regexp, printer=name))
                common.resolve_all_printers(session)
                session.commit()
        elif action == 'reprint':
            no = int(printout_id)
            common.try_to_print(session.query(model.DbPrintout).filter(model.DbPrintout.id == no).one())
            session.commit()
        if action:
            raise cherrypy.HTTPRedirect("main")

        template = self._lookup.get_template('admin.html')
        return template.render(service_enabled=self._iprinter_server.enabled, session=cherrypy.request.db)


def clear_text(mypass):
    return mypass


def main():

    #print current_dir

    conf = {
        '/admin': {
            'tools.basic_auth.on': True,
            'tools.basic_auth.realm': 'iPrinter',
            'tools.basic_auth.users': {'admin': '111111'},
            'tools.basic_auth.encrypt': clear_text
        },
        '/admin/data': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(current_dir, "printouts")
        },
        'global': {
            'server.socket_host': '0.0.0.0',
            'server.socket_port': 8085,
            'tools.encode.on': True,
            'tools.encode.encoding': "utf8",
            'tools.db.on': True
        },
        '/content': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': os.path.join(current_dir, "content")
        },
    }

    root = IPrinterServer()
    root.admin = AdminServer(root)

    SAEnginePlugin(cherrypy.engine, 'storage.sqlite').subscribe()
    cherrypy.tools.db = SATool()

    cherrypy.quickstart(root, '/', config=conf)

    #cherrypy.quickstart(IPrinterServer(), config=config_file_name)
    #cherrypy.engine.start()
    #cherrypy.engine.block()
if __name__ == "__main__":
    main()

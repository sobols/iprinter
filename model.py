# SQLAlchemy
from sqlalchemy.orm import sessionmaker, relationship, backref, composite
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Float, String, Boolean, DateTime, ForeignKey, create_engine
DbBase = declarative_base()


# ORM Types

class DbPrintout(DbBase):
    __tablename__ = 'printout'

    id           = Column(Integer, primary_key=True)
    ip_address   = Column(String)
    hostname     = Column(String)
    length       = Column(Integer)
    timestamp    = Column(DateTime)
    cur_printer  = Column(String)
    used_printer = Column(String)

class DbPrinterChoosingRule(DbBase):
    __tablename__ = 'printer_choosing_rule'

    id                 = Column(Integer, primary_key=True)
    hostname_regexp    = Column(String)
    printer            = Column(String)
